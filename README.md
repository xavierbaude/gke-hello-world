[![Docker Repository on Quay](https://quay.io/repository/xavierbaude/hello-world/status "Docker Repository on Quay")](https://quay.io/repository/xavierbaude/hello-world)

# gke-hello-world
Hello World project with GKE, Helm, Ingress Controller, Cert Manager...

#Order to deploy :
  helm
  nginx-ingress
  cert-manager
  hello-world
